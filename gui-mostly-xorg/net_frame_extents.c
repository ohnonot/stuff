/* Create window, retrieve frame extents via xprop command,
 * close window.
 *
 * Usage:
 *   gcc -o xlib_hello xlib_hello.c -lX11
 *   ./xlib_hello
 */
#include <X11/Xlib.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char** argv) {
    Display* disp = XOpenDisplay(NULL);
    if (disp == NULL) {
        return 1;
    }

    int screen = DefaultScreen(disp);
    char name[24] = "x_get_net_frame_extents";
    GC gc = DefaultGC(disp, screen);

    Window parent_window = DefaultRootWindow(disp);

    int x = -0;
    int y = -0;
    unsigned int width = 1;
    unsigned int height = 1;
    unsigned int border_width = 0;
    unsigned int border_color = BlackPixel(disp, screen);
    unsigned int background_color = BlackPixel(disp, screen);

    // Create window
    Window win = XCreateSimpleWindow(disp, parent_window,
                                              x,
                                              y,
                                              width,
                                              height,
                                              border_width,
                                              border_color,
                                              background_color);
    // Make window visible
    XMapWindow(disp, win);
    // Set window name
    XStoreName(disp,win,name);
    // Get WM_DELETE_WINDOW atom
    Atom wm_delete = XInternAtom(disp, "WM_DELETE_WINDOW", True);

///////////////////////////////////////////////////////////////////////////
    char command[70] = "xprop -notype -name ";
    strcat(command,name);
    strcat(command," _NET_FRAME_EXTENTS");
    system(command);
///////////////////////////////////////////////////////////////////////////

    // Subscribe WM_DELETE_WINDOW message
    XSetWMProtocols(disp, win, &wm_delete, 1);
    XCloseDisplay(disp);
    return 0;
}
