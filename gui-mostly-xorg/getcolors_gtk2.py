#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-

# thanks to this:
# bytes.com/topic/python/answers/748007-pygtk-theme-colors

#~ import pygtk
#~ pygtk.require('2.0')
import gtk
#~ import sys

#~ print 'Number of arguments:', len(sys.argv), 'arguments.'
#~ print 'Argument List:', str(sys.argv[1])

w = gtk.Window()
w.realize()
style=w.get_style()
#~ for attr in w.get_style():
	#~ print attr
sep="============================================"

print '''A style contains the graphics information needed by a widget to draw itself in its various states:

  STATE_NORMAL        # The state during normal operation.
  STATE_ACTIVE        # The widget is currently active, such as a button pushed
  STATE_PRELIGHT      # The mouse pointer is over the widget.
  STATE_SELECTED      # The widget is selected
  STATE_INSENSITIVE   # The widget is disabled

A style contains the following attributes:
'''

l=[gtk.STATE_NORMAL,gtk.STATE_ACTIVE,gtk.STATE_PRELIGHT,gtk.STATE_SELECTED,gtk.STATE_INSENSITIVE]
#~ see http://www.pygtk.org/pygtk2tutorial/sec-WidgetStyles.html for a full list

print sep,"\nbase - a list of 5 colors"
for i in l:
	print "\t",style.base[i].to_string(),i

print sep,"\ntext - a list of 5 colors"
for i in l:
	print "\t",style.text[i].to_string(),i

print sep,"\nfg - a list of 5 foreground colors - one for each state"
for i in l:
	print "\t",style.fg[i].to_string(),i

print sep,"\nbg - a list of 5 background colors"
for i in l:
	print "\t",style.bg[i].to_string(),i

print sep,"\nlight - a list of 5 colors - created during set_style() method"
for i in l:
	print "\t",style.light[i].to_string(),i

print sep,"\ndark - a list of 5 colors - created during set_style() method"
for i in l:
	print "\t",style.dark[i].to_string(),i

print sep,"\nmid - a list of 5 colors - created during set_style() method"
for i in l:
	print "\t",style.mid[i].to_string(),i

print sep,"\ntext_aa - a list of 5 colors halfway between text/base"
for i in l:
	print "\t",style.text_aa[i].to_string(),i
#~ print "============================================"
#~ for i in l:
	#~ print "BG  \t:: black\t:: ",style.black[i].to_string(),i
#~ print "============================================"
#~ for i in l:
	#~ print "BG  \t:: white\t:: ",style.white[i].to_string(),i
#~ print "============================================"
#~ for i in l:
	#~ print "BG  \t:: font_desc\t:: ",style.font_desc[i].to_string(),i
#~ print "============================================"
#~ for i in l:
	#~ print "BG  \t:: xthickness\t:: ",style.xthickness[i].to_string(),i
#~ print "============================================"
#~ for i in l:
	#~ print "BG  \t:: fg_gc\t:: ",style.fg_gc[i].to_string(),i
#~ print "============================================"
#~ for i in l:
	#~ print "BG  \t:: bg_gc\t:: ",style.bg_gc[i].to_string(),i
#~ print "============================================"
#~ for i in l:
	#~ print "BG  \t:: light_gc\t:: ",style.light_gc[i].to_string(),i
#~ print "============================================"
#~ for i in l:
	#~ print "BG  \t:: dark_gc\t:: ",style.dark_gc[i].to_string(),i
#~ print "============================================"
#~ for i in l:
	#~ print "BG  \t:: mid_gc\t:: ",style.mid_gc[i].to_string(),i
#~ print "============================================"
#~ for i in l:
	#~ print "BG  \t:: text_gc\t:: ",style.text_gc[i].to_string(),i
#~ print "============================================"
#~ for i in l:
	#~ print "BG  \t:: base_gc\t:: ",style.base_gc[i].to_string(),i
#~ print "============================================"
#~ for i in l:
	#~ print "BG  \t:: black_gc\t:: ",style.black_gc[i].to_string(),i
#~ print "============================================"
#~ for i in l:
	#~ print "BG  \t:: white_gc\t:: ",style.white_gc[i].to_string(),i
#~ print "============================================"
#~ for i in l:
	#~ print "BG  \t:: bg_pixmap\t:: ",style.bg_pixmap[i].to_string(),i

#~ for i in l:
	#~ print "BASE\t::\t:: ",style.base[i]
#~ for i in l:
	#~ print "- text",i,"\t:: ",style.text[i]
#~ for i in l:
	#~ print "FG  \t::\t:: ",style.fg[i]
#~ for i in l:
	#~ print "BG  \t::\t:: ",style.bg[i]

#~ if sys.argv[1] == 'fg':
	#~ print "\t",style.fg[gtk.STATE_NORMAL]
#~ if sys.argv[1] == 'bg':
	#~ print "\t",style.bg[gtk.STATE_NORMAL]

#~ print "\t",style.fg[gtk.STATE_NORMAL].to_string(),i
#~ print "\t",style.bg[gtk.STATE_NORMAL].to_string(),i
#~ print "\t",style.bg[gtk.STATE_SELECTED]

#~ print "\t",style.fg[gtk.STATE_NORMAL].to_string(),i
#~ print "\t",style.bg[gtk.STATE_NORMAL].to_string(),i

