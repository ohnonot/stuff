# tileset

## An image viewer and background setter, with a focus on small tileable images

Get a better impression of tiled images while browsing through
folders full of them, recursively.  
Change the background color (for partly transparent tiles).  
Apply effects to a screen-sized canvas created from the current tile.  
All results can be set as the desktop background with **feh** (TODO: include other
background setters).

## Example screenshot

[tileset in action](http://dt.iki.fi/stuff/git/tileset/tileset-screenshot-01.png) (the background was also created with tileset).

## Usage

`tileset [path/to/folder]`

You may pass a different path as an argument, otherwise the current folder will
be searched for images, recursively.

Hidden files and folders are excluded by default.  
Can be overridden by setting the `parse_hidden` variable to 1.

You can add a symlink to your `$HOME/bin` (if it's in your $PATH):  
`cd ~/bin; ln -s /path/to/git/tileset/tileset`

Single image mode:  
`tileset [path/to/folder/image.ext]`  
This way you can open a single image with tileset, e.g. from your filemanager.  
Copy `tileset.desktop` to `~/.local/share/applications/`. Requires `tileset` to be in your `$PATH`.

## Dependencies

- `imagemagick`
- `feh`
- `yad`
- `find shuf which`

### Optional but recommended

- `wmctrl`
- `/tmp` has to be user writable. Otherwise, adjust the `tmpdir` variable in the script.
- `ncurses` for nicer user interaction

## Also see

[this blog post](http://dt.iki.fi/download-filetype-website) on how to
obtain a nice collection of transparent tiles.
