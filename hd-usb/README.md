# Completely switch off a permanently connected USB hard drive

Also see [this article](https://dt.iki.fi/disable-usb-drive).