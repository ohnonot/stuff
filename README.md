# stuff

Contains scripts and other useful stuff that does not require its own repository.

Explanations inside the files.

## Licensing

Everything in here is under the license described in the LICENSE file, unless otherwise mentioned inside the script.
