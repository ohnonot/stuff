### [xmountaincolors](https://notabug.org/ohnonot/xmountaincolors)

[xmountains][1], but with configurable colors

Here's a [gallery with examples](http://dt.iki.fi/galleries/xmountaincolors).

#### Usage, where it differs from xmountains:

    ./xmountaincolors -N [theme]

or

    man ./xmountaincolors.man


Themes are to be stored in `$HOME/.config/xmountaincolors/[theme].conf`.
If a theme is not found or does not contain all configuration options or if its
syntax is faulty, xmountaincolors falls back on the built-in defaults.

Theme syntax:

    name = "example theme, some description";
    colors =
    {
    base      = ( { red = 1.0; green = 0.5; blue = 0.5; }, { red = 0.6; green = 0.5; blue = 0.5; }, { red = 0.6; green = 0.5; blue = 0.5; } );
    black     = ( { red = 0.132435467; green = 0.33; blue = 0.36; } );
    white     = ( { red = 0.3; green = 0.33; blue = 0.36; } );
    sky       = ( { red = 0.35; green = 0.38; blue = 0.41; } );
    sea       = ( { red = .3; green = .33; blue = .36; } );
    };
    // Some comment
    // All values are floats. Please note that e.g. blue=1; is not valid, it has to be blue=1.0;

If a value is left empty or not recognized, config reading fails and the built-in
default colors are used instead. It is however possible to define only some of
the arrays base, black, white, sky and sea (but the script provided is not
capable of producing this type of partial configs).

The `base` colors define how the 3 different altitudes are colored, from bottom
to top. The rest is self-explanatory, I think. Just try a few themes, you will
see what happens :-)

#### Tools included

Since the config file syntax is overly complex, I included the script `mktheme`
that will generate theme files. All you need to do is enter the colors. If you
have [grabc](http://www.muquit.com/muquit/software/grabc/grabc.html) installed,
the script will automatically prompt you to either use that or enter the values
manually.

#### Installation

Depends on `imake` (just like the original xmountains) and `libconfig` (this is
new).

* On **Debian**, _building_ this modified version of xmountains depends on the
packages `xutils-dev`, `libconfig-dev` and `xbitmaps` (and maybe others). The
program itself additionally depends on `libconfig9`. Also refer to
[deb/control](deb/control).

* On **ArchLinux**, see this [PKGBUILD](https://aur.archlinux.org/cgit/aur.git/tree/PKGBUILD?h=xmountains)
for more info, and additionally install `libconfig`.

Please `cd` to the `src` directory and invoke `xmkmf` (part of `imake`), then
`make`.

Once it's made, you can run it straight from there, e.g.:

    ./xmountaincolors -b -M -r 1 -N bleak

But first you should create the directory `~/.config/xmountaincolors` (this is
not configurable atm) and copy some themes into it. Example themes are provided.

[1]: http://www2.epcc.ed.ac.uk/~spb/xmountains/about_xmountains.html